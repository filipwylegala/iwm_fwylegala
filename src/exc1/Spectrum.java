package exc1;

import java.sql.Date;

import javax.management.Descriptor;

public class Spectrum<T> extends Sequence<T>{
    //zmienna scaling rodzaj skali log lub liniowa

    double scaling;
    public String device;
    Spectrum(){
        super();
        scaling=1231;
        device="auto";
    }


    Spectrum(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer, double scaling){
        super(device,  description,  date,  channelNr,  unit,  resolution, buffer);
        this.scaling=scaling;
        this.device=device;


    }
    public String toString(){
        return "\n Scaling:"+scaling+ "\n ===="; 
        
    }
public void info(){
    System.out.println(toString());
}
}
