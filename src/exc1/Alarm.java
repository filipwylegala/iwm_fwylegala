package exc1;

public class Alarm extends Packet {
    int channelNr;
    double threshold;
    int direction;
    
    Alarm(){
        super();
        channelNr=0;
        threshold=0;
        direction=0;
    }
    Alarm(String device, String description, long date, int channelNr, double threshold , int direction){
        super(device, description, date);
        this.channelNr=channelNr;
        this.threshold=threshold;
        this.direction=direction;
    }
    //tostring trzeba 
    public String toString() {
		return super.toString() + "\n Numbere of channel: " +  channelNr + "\n Treshhold: "+threshold+"\n direction:"+direction+ "\n ----";
    }
    public void info(){
        System.out.println(toString());
}
}
