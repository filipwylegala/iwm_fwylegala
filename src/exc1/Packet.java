package exc1;

import java.sql.Time;
import java.util.Date;

//tworzenie pakietu danych klasy abstrakcyjnej
public abstract class Packet {
    // zmienne typu chronionego
    protected String device;
    protected String description;
    protected long date;

   public Packet(){
        device="";
        description="";
        date= new Date().getTime();
    }
    public Packet(String device, String description, long date ){
        this.device=device;
        this.description=description;
        this.date=date;
    }
    public String toString(){
        return "\n device name:"+device+"\n description of the device:"+description+"\n Time in seconds:"+date + "\n ===="; 
        
    }
public void info(){
    System.out.println(toString());
}
    
}
