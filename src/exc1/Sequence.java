package exc1;

import java.nio.Buffer;

public abstract class Sequence<T> extends Packet{

    protected int channelNr;
    protected String unit;
    protected double resolution;
    protected T[] buffer = (T[]) new Object[7];
    
    Sequence(){
        super();
        channelNr=5;
        unit="M/2";
        resolution=12;
    }
    Sequence(String device, String description, long date, int channelNr, String unit, double resolution,T[] buffer){
    super(device, description, date);
    this.channelNr=channelNr;
    this.unit=unit;
    this.resolution = resolution;	
    this.buffer =buffer;
    }
    public String toString(){
      return "\n Channel number:"+channelNr+"\n Jednostka:"+unit+"\n rozddzielczość:"+resolution + "\n buffer"+ buffer+ "\n ===="; 
      
  }
public void info(){
  System.out.println(toString());
}


}
