package exc1;

public class TimeHistory<T> extends Sequence<T>{
    //czułość napięciowa 
    double sensitivity;
    //konstruktor
    TimeHistory(){
        //dziedziczenie
        super();
        sensitivity=98;
    }

    TimeHistory(String device, String description, long date,int channelNr, String unit, double resolution, T[] buffer, double sensitivity){
        super(device, description,date, channelNr, unit, resolution, buffer);
        this.sensitivity=sensitivity;
    }
    public String toString(){
        return "\n Sensitivity:"+sensitivity+ "\n ===="; 
        
    }
public void info(){
    System.out.println(toString());
}

}
