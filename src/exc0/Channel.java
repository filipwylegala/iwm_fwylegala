package exc0;

import java.util.Arrays;
import java.util.Random;
import java.lang.Math;
public class Channel{
    private int channelNr = 1;
        private String channelName = "1";
        private int bits = 12;
        private double rangeMin = 5;
        private double rangeMax = -5;
        private double sensitivity = 14565;
        private String unit ="cm/s";
        private double samplingFreq = 175;
        private int lenght =20;
        private short[] samples = new short[lenght];


 Channel(){
    super();
    short minval= 1;
    short maxval= 24;
    Random rand=new Random();
    for(int i = 0; i < samples.length; i++){
        samples[i] = (short)(rand.nextInt((maxval-minval + 1) + 1));
        System.out.println(samples[i]);
    }
    

}
//konstruktor z parametrami
public Channel(int channelNr, String channelName, int bits, double rangeMin, double rangeMax, double sensitivity, String unit, double samplingFreq,int lenght, Short[] samples  ){
    super();
    this.channelNr = channelNr;
    this.channelName =channelName;
    this.bits = bits;
    this.rangeMin = rangeMin;
    this.rangeMax = rangeMax;
    this.sensitivity = sensitivity;
    this.unit = unit;
    this.samplingFreq = samplingFreq;
    this.lenght =lenght;
    this.samples = new short[lenght];
}
//oblicz wartosc probki
public  double value(int numProbe){ 
double dA=(rangeMax-rangeMin)/Math.pow(2., bits);
double v =(rangeMin+dA*samplingFreq)*sensitivity;
return v;
}
//oblicz srednia
public double avaerage(){
double sum=0;
for (int i=0; i<lenght; i++){
    sum += samples[i];

}
double mean= sum/lenght;
return mean;
}
//oblicz rms
public double rms(){
double square= 0;
double mean=0;
double root =0;
    for (int i=0; i<lenght; i++){
        square += Math.pow(samples[i], 2);  
    }
    mean=(square/lenght);
    root = Math.sqrt(mean);
    return root;
}
public String toString(){
    return super.toString() + "\n Channel [Nr = " + channelNr + ",\n Name = " + channelName + ",\n Sensitivity = " + sensitivity +
    ",\n Unit = " + unit + ",\n Bits = " + bits +",\n RangeMin = " + rangeMin +
    ",\n RangeMax = " + rangeMax +",\n SamplingFreq = " + samplingFreq +",\n Length = " + lenght +
    ",\n Samples = " + Arrays.toString(samples) + ",\n Mean = " + avaerage() + ",\n RMS = " + rms() + "]" + "\n------";

    
}
public void info(){
    System.out.println(toString());


}
}


